## Model organization


### Base model 

Base model is [Heck Residence.blend](Heck Residence.blend). 
It is modelled with BlenderBIM but currently the companion IFC file seems broken 
so no IFC export possible. As a result, it was loosely modelled without any particular
BlenderBIM technique. Just make sure:

    1. **Objects are solid**: There is no hole (missing face), no overlapping face,
       no manifold edge (edit mode -> edge mode -> select -> by trait -> non-manifold)
       
    2. **Objects are meaningfully named**: Instead of Plane034, rename objects to
       something like "Wall034"
       
    3. **Use IFC classes**: Either attribute a class with BlenderBIm's Properties
       -> Object Properties -> IFC Class, or simply name your objects with the 
       class first, for ex. "IfcWall/Wall034" (this works with the Blender/FreeCAD
       explorer, but not sure if it will still work with IFC export)
       
    4. **Use collections**: Group your objects into collections and sub-collections
       in a meaningful, practical way, like you would organize files in directories.
       Make sure there is at least one collection for each "Part" that you want to
       export as a separate FreeCAD file.


### IFC export

Currently not working


### Direct FreeCAD export

To export your model directly as a FreeCAD file, you first need the FreeCAD exporter:

    * Install FreeCAD
    * Make sure the Python version that Blender uses is the same version that FreeCAD
      uses. This is very important. Both Python versions are found when opening the Python
      console in each application. Only the first two numbers matter. For example, 3.8.2
      is compatible with 3.8.6 but not with 3.9.1
    * Install the FreeCAD exporter for Blender from 
      [this gist](https://gist.github.com/yorikvanhavre/029f6fcce9f4d0e62fb6163804b7f80d).
      Press the RAW button -> save as .py file, then from Blender Edit -> Preferences ->
      Addons -> Install...
    * Locate your FreeCAD.so (or FreeCAD.pyd on Windows) (use your OS's search tools).
      In the addon preferences, indicate the path of your FreeCAD.so.

Then, isolate or select the objects of one collection that you want to export as
a FreeCAD file, go to File -> Export -> FreeCAD .FCStd, make sure the scale is 
set to 1000 (Blender models are traditionally in meters, even if working in inches/
feet, while in FreeCAD the default unit is millimeter), set the precision to 4 and
enable compounds. This places all the exported objects into a compound, which is
convenient later on.

Save the different parts of your model as different FreeCAD files, so they can
be updated indivdiually without the need to reimport the whole model.

### Mesh export

If for some reason it is not possible to use the FreeCAD exporter, then you can 
export your model as .OBJ or .DAE. Then, import that file into FreeCAD, select
all the objects, then in workbench Part -> Create Part from Mesh. You will
probably need to scale the objects by 1000, though.

Place all the Part objects inside a Compound.

Save the different parts of your model as different FreeCAD files, so they can
be updated indivdiually without the need to reimport the whole model.


### FreeCAD assembly + create views

FreeCAD model [Heck Residence.FCStd](Heck Residence.FCStd) contains sections 
and sheets, It uses the above patial files as reference.  All commands below are 
in the BIM workbench.

In FreeCAD, create a blank file that will contain your assembled model and the 2D
views. Use the Arch Reference tool to import the "Compound" object of each partial
model file.

Creating views and sheets is done by:

    * Place section planes in your model. Make sure each section plane considers
      all and only the necessary model parts.
    
    * Create Shape2Dviews for each section plane. Create one Shape2Dview in "Solid"
      mode for the viewed geometry, then another one in "Cut Faces" or "Cut Lines"
      mode for the cut lines. Make sure the "In Place" property is turned off and move
      and rotate the cut lines into place if needed.
      
    * Place the Shape2Dviews into a group, and add all the necessary linework,
      dimensions and texts into that group.
      
    * Create a TechDraw sheet
    
    * With the above group selected, create a TechDraw Draft view and adjust the
      scale.



