### Code research

* Location: [709 Oneida Pl, Madison](https://www.cityofmadison.com/assessor/property/propertydata.cfm?ParcelN=070928314031)
* Zone: Traditional Residential - Consistent District 1  ([TR-C1](https://www.cityofmadison.com/assessor/documents/ZoningDistricts.pdf)) - Chapter 28 of zoning code, page 30
* Lot size: 8712 sqft
* Lot front dimension: 81' (from GIS) or 94' (parcel information above)
* Lot side dimension: 117' (from GIS) or92' (lot size / parcel info)
* Front Yard Setback: 20'
* Side Yard Setback: One-story: 6', Two-story: 7'
* Maximum lot coverage: 50%
* Minimum usable open space 1000 sqft per dwelling unit
* Rear Yard Setback: Lesser of 30% lot depth or 35. If the existing principal structure and any additions to it, covers twenty percent (20%) or less of the lot area, the rear yard setback may be reduced by twenty-five percent (25%).



### Current situation

* Front yard setback: 25' **OK**
* Side yard setback: 9' **OK**
* Rear yard setback: 51' **OK**
* Current lot coverage: 1583 sqft = 18% **OK**
* Current usable open space: 8712 - (1583 + 480 + 250) = 6399 sqft **OK**



### Proposed situation:

* Front yard setback: unchanged
* Side yard setback: unchanged
* Read yard setback: 45' **OK - minimum is 30'**
* New lot coverage: 1583 + 318 = 1901 sqft = 22% **OK - maximum is 50%**
* New usable open space: 6399 - 284 = 6081 sqft **OK - minimum is 1000**

